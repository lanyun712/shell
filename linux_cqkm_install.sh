#!/bin/bash
set -e
echo "更多帮助详见：https://liangbi.gitee.io"
echo "现在开始下载码表，临时存放到当前目录"
curl -# -SL https://gitee.com/lanyun712/cqeb/attach_files/860243/download/linux_rime_cqkm_0.5.7.1.tar.gz|tar zx
echo "码表下载完成，现在清理的旧码表"
echo "本次操作需要「管理员权限」，请输入密码"

TS=`date +%Y%m%d-%H%M%S`

DN=$HOME/.config/fcitx/rime
DM=$HOME/.config/ibus/rime
DK=$HOME/.local/share/fcitx5/rime

DR=/usr/share/rime-data

if [ -d $DR ];
then
  sudo cp -rf linux_rime_cqkm_0.5.7.1/* $DR
else
  echo "未检测到 Rime，请先安装 Rime 输入法引擎。程序退出。"
  exit
fi

for d in $DN $DM $DK; do
  [ -d "$d" ] && {
    echo "Backup $d to $d-$TS ..."
    mv $d $d-$TS
  }
  mkdir -p -m 755  `dirname $d`
  cp -a linux_rime_cqkm_0.5.7.1 $d
done

echo "中州韻配置文件安装成功，注銷或重启系統後即可使用了。"

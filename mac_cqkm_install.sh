#!/bin/bash

path=${0%/*}
echo "本此为增量更新，会清理鼠须管的旧码表，如有自定义码表请注意先备份"
echo "更多帮助详见：https://liangbi.gitee.io"
echo "现在开始下载码表，临时存放到当前目录"
curl -# -SL https://gitee.com/lanyun712/cqeb/attach_files/860239/download/mac_rime_cqkm_0.5.7.1.zip|tar zx
echo "码表下载完成，现在清理的旧码表"
echo "本次操作需要「管理员权限」，请输入密码"
sudo rm -rf /Library/Input\ Methods/Squirrel.app/Contents/SharedSupport/*
echo "接下来清理鼠须管的旧缓存数据"
sudo rm -rf ~/Library/Rime/*
echo "将新的码表放到鼠须管主程序内"
sudo cp -rf mac_rime_cqkm_0.5.7.1/* /Library/Input\ Methods/Squirrel.app/Contents/SharedSupport
echo "向用户目录转移文件"
sudo cp -rf mac_rime_cqkm_0.5.7.1/* ~/Library/Rime
sudo rm -rf /Library/Input\ Methods/Squirrel.app/Contents/SharedSupport/build
sudo chmod -R 777 ~/Library/Rime
echo "重新部署，使新码表生效"
/Library/Input\ Methods/Squirrel.app/Contents/MacOS/Squirrel --reload
echo "做完，收工。"
sudo chmod -R 777 ~/Library/Rime
bash read -p 'Press any key to Exit'
